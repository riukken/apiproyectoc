﻿using ProyectoMonedero.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ProyectoMonedero.Controllers
{
    [EnableCors(origins: "http://localhost:3000", headers: "*", methods: "*")]


    public class FileUploadingController : ApiController
    {

        private monederoEntities db = new monederoEntities();

        [HttpPost]
        [Route("api/FileUploading")]
        public async Task<string> UploadFile(int idusuario)
        {
            var ctx = HttpContext.Current;
            var root = ctx.Server.MapPath("~/images");
            var provider = new MultipartFormDataStreamProvider(root);

            try
            {
                await Request.Content.ReadAsMultipartAsync(provider);

                foreach(var file in provider.FileData)
                {
                    var name = file.Headers.ContentDisposition.FileName;

                    //remove double quotes from string
                    name = name.Trim('"');

                    var localFileName = file.LocalFileName;
                    var filePath = Path.Combine(root, idusuario + name);
                    File.Move(localFileName, filePath);

                    
                    /*Buscamos la idusuario que nos llega en clientes y si esta guardamos el archivo nuevo. 
                     *Si hay alguna imagen guardada la asiganmos en una string para luego borrarla y guardar la nueva.*/

                    cliente cliente = db.clientes.Find(idusuario);
                    string fotoAnterior = cliente.img;
                    cliente.img = idusuario+name;

                        db.SaveChanges();

                    if (fotoAnterior != "" && fotoAnterior != null)
                    {
                        File.Delete(Path.Combine(root, fotoAnterior));
                    }

                }
            }
            catch(Exception e)
            {
                return "Error"+e.Message;
            }

                return "File Uploaded";
        }
    }
}
