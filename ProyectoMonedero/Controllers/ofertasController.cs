﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using ProyectoMonedero.Models;

namespace ProyectoMonedero.Controllers
{
    //CORS
    //origen que queremos permitir headers y methods
    [EnableCors(origins: "http://localhost:3000", headers: "*", methods: "*")]
    public class ofertasController : ApiController
    {
        private monederoEntities db = new monederoEntities();

        // GET: api/ofertas
        public IQueryable<oferta> Getofertas()
        {
            return db.ofertas;
        }

        // GET: api/ofertas/5
        [ResponseType(typeof(oferta))]
        public IHttpActionResult Getoferta(int id)
        {
            oferta oferta = db.ofertas.Find(id);
            if (oferta == null)
            {
                return NotFound();
            }

            return Ok(oferta);
        }

        // PUT: api/ofertas/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putoferta(int id, oferta oferta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != oferta.idOferta)
            {
                return BadRequest();
            }

            db.Entry(oferta).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ofertaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ofertas
        [ResponseType(typeof(oferta))]
        public IHttpActionResult Postoferta(oferta oferta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ofertas.Add(oferta);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = oferta.idOferta }, oferta);
        }

        // DELETE: api/ofertas/5
        [ResponseType(typeof(oferta))]
        public IHttpActionResult Deleteoferta(int id)
        {
            oferta oferta = db.ofertas.Find(id);
            if (oferta == null)
            {
                return NotFound();
            }

            db.ofertas.Remove(oferta);
            db.SaveChanges();

            return Ok(oferta);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ofertaExists(int id)
        {
            return db.ofertas.Count(e => e.idOferta == id) > 0;
        }
    }
}