﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ProyectoMonedero.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        //GET Consultar
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        //POST añadir
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        //PUT actualizar
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        //DELETE eliminar
        public void Delete(int id)
        {
        }
    }
}
