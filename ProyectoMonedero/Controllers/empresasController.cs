﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using ProyectoMonedero.Models;

namespace ProyectoMonedero.Controllers
{
    //CORS
    //origen que queremos permitir headers y methods
    [EnableCors(origins: "http://localhost:3000", headers:"*", methods:"*")]
    public class empresasController : ApiController
    {
        private monederoEntities db = new monederoEntities();

        // GET: api/empresas
        public IQueryable<empresa> Getempresas()
        {
            return db.empresas;
        }

        // GET: api/empresas/5
        [ResponseType(typeof(empresa))]
        public IHttpActionResult Getempresa(int id)
        {
            empresa empresa = db.empresas.Find(id);
            if (empresa == null)
            {
                return NotFound();
            }

            return Ok(empresa);
        }

        // PUT: api/empresas/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putempresa(int id, empresa empresa)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != empresa.id)
            {
                return BadRequest();
            }

            db.Entry(empresa).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!empresaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/empresas
        [ResponseType(typeof(empresa))]
        public IHttpActionResult Postempresa(empresa empresa)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.empresas.Add(empresa);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = empresa.id }, empresa);
        }

        // DELETE: api/empresas/5
        [ResponseType(typeof(empresa))]
        public IHttpActionResult Deleteempresa(int id)
        {
            empresa empresa = db.empresas.Find(id);
            if (empresa == null)
            {
                return NotFound();
            }

            db.empresas.Remove(empresa);
            db.SaveChanges();

            return Ok(empresa);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool empresaExists(int id)
        {
            return db.empresas.Count(e => e.id == id) > 0;
        }
    }
}